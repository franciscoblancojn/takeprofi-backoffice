import f_money from "@/functions/money";
export const money = f_money;

import f_log from "@/functions/log";
export const log = f_log;

import f_generatePassword from "@/functions/generatePassword";
export const generatePassword = f_generatePassword;

import f_expireToken from "@/functions/expireToken";
export const expireToken = f_expireToken;