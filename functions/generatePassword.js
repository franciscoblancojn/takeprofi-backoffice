export default () => {
    return (
        Math.random().toString(36).slice(-8).toLocaleUpperCase() +
        Math.random().toString(36).slice(-8) +
        (parseInt(Math.random() * 100))
    );
};
