export default {
    bancolombia: {
        text: "Bancolombia",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/Bancolombia.png"
                    alt="Bancolombia"
                    className="mr-5"
                />
                Bancolombia
            </div>
        ),
    },
    bancoDeBogota: {
        text: "Banco de Bogota",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/bancoDeBogota.png"
                    alt="Banco de Bogota"
                    className="mr-5"
                />
                Banco de Bogota
            </div>
        ),
    },
    avVillas: {
        text: "Av Villas",
        option: (
            <div className="flex  flex-no-wrap flex-align-center">
                <img
                    src="/banks/avVillas.png"
                    alt="Banco de Bogota"
                    className="mr-5"
                />
                Av Villas
            </div>
        ),
    },
    bancoPopular: {
        text: "Banco Popular",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/bancoPopular.png"
                    alt="Banco Popular"
                    className="mr-5"
                />
                Banco Popular
            </div>
        ),
    },
    BBVA: {
        text: "BBVA",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img src="/banks/BBVA.png" alt="BBVA" className="mr-5" />
                BBVA
            </div>
        ),
    },
    bancoPichincha: {
        text: "Banco Pichincha",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/bancoPichincha.png"
                    alt="Banco Pichincha"
                    className="mr-5"
                />
                Banco Pichincha
            </div>
        ),
    },
    tether: {
        text: "USDT - Tether / Cripto Monedas",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/tether.png"
                    alt="USDT - Tether / Cripto Monedas"
                    className="mr-5"
                />
                USDT - Tether / Cripto Monedas
            </div>
        ),
    },
    bancoCajaSocial: {
        text: "Banco Caja Social",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/bancoCajaSocial.png"
                    alt="Banco Caja Social"
                    className="mr-5"
                />
                Banco Caja Social
            </div>
        ),
    },
    bancoItau: {
        text: "Banco Itaú",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/bancoItau.png"
                    alt="Banco Itaú"
                    className="mr-5"
                />
                Banco Itaú
            </div>
        ),
    },
    bankOfAmerica: {
        text: "Bank of America",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/bankOfAmerica.png"
                    alt="Bank of America"
                    className="mr-5"
                />
                Bank of America
            </div>
        ),
    },
    zelle: {
        text: "Zelle",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img src="/banks/zelle.png" alt="Tether" className="mr-5" />
                Zelle
            </div>
        ),
    },
    davivienda: {
        text: "Davivienda",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/davivienda.png"
                    alt="Davivienda"
                    className="mr-5"
                />
                Davivienda
            </div>
        ),
    },
    bancoDeOccidente: {
        text: "Banco de Occidente",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/bancoDeOccidente.png"
                    alt="Banco de Occidente"
                    className="mr-5"
                />
                Banco de Occidente
            </div>
        ),
    },
    scotiaBankColpatria: {
        text: "Scotia Bank - Colpatria",
        option: (
            <div className="flex flex-no-wrap flex-align-center">
                <img
                    src="/banks/scotiaBankColpatria.png"
                    alt="Scotia Bank - Colpatria"
                    className="mr-5"
                />
                Scotia Bank - Colpatria
            </div>
        ),
    },
};
