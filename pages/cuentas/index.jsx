import React, { useState, useEffect } from "react";
import Link from "next/link";
import exportFromJSON from "export-from-json";

import getToken from "@/servers/getToken";

import { accounts } from "@/api/api";
import { log } from "@/functions/_index";

import Content from "@/components/content";
import Breadcrumbs from "@/components/breadcrumbs";
import Space from "@/components/space";
import Table from "@/components/table";

const TABLEHEAD = [
    {
        id: "_id",
        html: "ID",
        print: (e) => {
            return (
                <Link href={`/cuentas/${e}`}>
                    <a className="font-size-16">{e}</a>
                </Link>
            );
        },
    },
    {
        id: "name",
        html: "Nomber",
        print: (e) => e,
    },
    {
        id: "last_name",
        html: "Apellido",
        print: (e) => e,
    },
    {
        id: "email",
        html: "Email",
        print: (e) => {
            return (
                <a target="blank" href={`mailto:${e}`} className="font-size-16">
                    {e}
                </a>
            );
        },
    },
    {
        id: "phone",
        html: "Phone",
        print: (e) => {
            return (
                <a target="blank" href={`tel:${e}`} className="font-size-16">
                    {e}
                </a>
            );
        },
    },
    {
        id: "card",
        html: "Card",
        print: (e) => e,
    },
    {
        id: "pais",
        html: "Pais",
        print: (e) => e,
    },
    {
        id: "provincia",
        html: "Provincia",
        print: (e) => e,
    },
    {
        id: "ciudad",
        html: "Ciudad",
        print: (e) => e,
    },
    {
        id: "calle",
        html: "Calle",
        print: (e) => e,
    },
    {
        id: "role",
        html: "Rol",
        print: (e) => (e ? e : "client"),
    },
];
const ItemsBreadcrumbs = [
    {
        href: "/cuentas/",
        text: "Cuentas",
    },
];
const Index = ({ token, accountsRow }) => {
    log("ACCOUNTS",accountsRow,"aqua")
    const exportTable = () => {
        const fileName = new Date().toISOString().split(" ").join("_");
        exportFromJSON({ data: accountsRow, fileName, exportType: "xls" });
    };
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75 pb-20">
                <div className="col-12">
                    <Space space="110" />
                    <h1>Cuentas</h1>
                    <Space space="28" />
                </div>
                <div className="col-12 mb-48">
                    <div className=" flex flex-align-center flex-justify-between w-100">
                        <Breadcrumbs items={ItemsBreadcrumbs} />
                        <Link href="/cuentas/add">
                            <a>Crear Cuenta</a>
                        </Link>
                        {accountsRow.length !== 0 && (
                            <a
                                className="font-size-35 color-warm-grey-two flex flex-align-center flex-no-wrap"
                                onClick={exportTable}
                            >
                                <img
                                    src="/img/export.svg"
                                    alt="export"
                                    className="mr-5"
                                />
                                Exportar
                            </a>
                        )}
                    </div>
                </div>
                {accountsRow.length === 0 ? (
                    <>
                        <div className="col-12">
                            <h4>No hay cuentas</h4>
                        </div>
                    </>
                ) : (
                    <div
                        className="col-12"
                        style={{ maxWidth: "calc(100vw - 30px)" }}
                    >
                        <Table
                            rows={accountsRow}
                            ths={TABLEHEAD}
                            className="w-100"
                        />
                    </div>
                )}
            </div>
        </Content>
    );
};
export const getServerSideProps = async (ctx) => {
    const config = await getToken(ctx);
    const props = config.props;
    props.accountsRow = [];
    if (props.token) {
        const token = props.token;
        const result = await accounts.get(token);
        if (result.type === "ok") {
            const accountsRow = result.respond;

            log("ACCOUNTS",accountsRow,"aqua")
            props.accountsRow = accountsRow;
        }
    }

    return config;
};
export default Index;
