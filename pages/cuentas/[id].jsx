import React, { useState, useEffect } from "react";
import Link from "next/link";
import exportFromJSON from "export-from-json";

import getToken from "@/servers/getToken";

import { accounts, retreats, methods } from "@/api/api";

import { money, log } from "@/functions/_index";

import { banks, typeCuentas } from "@/const/_index";

import Content from "@/components/content";
import Breadcrumbs from "@/components/breadcrumbs";
import Space from "@/components/space";
import Table from "@/components/table";

const HEADACCOUNT = [
    {
        id: "_id",
        html: "ID",
        print: (e) => e,
    },
    {
        id: "name",
        html: "Nomber",
        print: (e) => e,
    },
    {
        id: "last_name",
        html: "Apellido",
        print: (e) => e,
    },
    {
        id: "email",
        html: "Email",
        print: (e) => {
            return (
                <a target="blank" href={`mailto:${e}`} className="font-size-16">
                    {e}
                </a>
            );
        },
    },
    {
        id: "phone",
        html: "Phone",
        print: (e) => {
            return (
                <a target="blank" href={`tel:${e}`} className="font-size-16">
                    {e}
                </a>
            );
        },
    },
    {
        id: "card",
        html: "Card",
        print: (e) => e,
    },
    {
        id: "pais",
        html: "Pais",
        print: (e) => e,
    },
    {
        id: "provincia",
        html: "Provincia",
        print: (e) => e,
    },
    {
        id: "ciudad",
        html: "Ciudad",
        print: (e) => e,
    },
    {
        id: "calle",
        html: "Calle",
        print: (e) => e,
    },
    {
        id: "role",
        html: "Rol",
        print: (e) => (e ? e : "client"),
    },
];
const TABLEHEADRETREATS = [
    {
        id: "date",
        html: "Fecha",
        print: (e) => {
            return new Date(e).toLocaleDateString();
        },
    },
    {
        id: "account",
        html: "Cuenta",
        print: (e) => {
            return (
                <div className="flex flex-no-wrap flex-align-center">
                    <img
                        src={`/banks/${e.bank}.png`}
                        alt={e.bank}
                        className="mr-10"
                    />
                    <div className="info align-left">
                        <strong className="name color-black-two font-size-14">
                            {e.name}
                        </strong>
                        <div className="number color-brownish-grey font-size-18">
                            {e.account}
                        </div>
                        <div className="type color-greyish-two font-size-16">
                            {typeCuentas[e.type]}
                        </div>
                    </div>
                </div>
            );
        },
    },
    {
        id: "periodo",
        html: "Periodo",
        print: (e) => {
            return `Periodo ${e}`;
        },
    },
    {
        id: "capital",
        html: "Capital Consolidado",
        print: (e) => {
            return money(e);
        },
    },
    {
        id: "monto",
        html: "Monto",
        print: (e) => {
            return money(e);
        },
    },
];
const TABLEHEADMETHODS = [
    {
        id: "name",
        html: "Nombre",
        print: (e) => e,
    },
    {
        id: "account",
        html: "Cuenta",
        print: (e) => e,
    },
    {
        id: "typeAccount",
        html: "Tipo",
        print: (e) => typeCuentas[e] || "",
    },
    {
        id: "bank",
        html: "Banco",
        print: (e) => banks[e].option || "",
    },
    {
        id: "date",
        html: "Fecha",
        print: (e) => {
            return new Date(e).toLocaleDateString();
        },
    },
];
const Index = ({ token, id, account, retreatsAccount, methodsAccount }) => {
    log("_id", id, "aqua");
    log("account", account, "aqua");
    log("RETREATS", retreatsAccount, "aqua");
    log("METHODS", methodsAccount, "aqua");
    const ItemsBreadcrumbs = [
        {
            href: "/cuentas/",
            text: "Cuentas",
        },
        {
            href: `/cuentas/${id}`,
            text: id,
        },
    ];
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75 pb-20">
                <div className="col-12">
                    <Space space="110" />
                    <h1>{account.email}</h1>
                    <Space space="28" />
                    <Breadcrumbs items={ItemsBreadcrumbs} />
                </div>
                <div className="col-12">
                    <ul>
                        {HEADACCOUNT.map((e, i) => {
                            return (
                                <li key={i}>
                                    <strong>{e.html}:</strong>
                                    {e.print(account[e.id])}
                                </li>
                            );
                        })}
                    </ul>
                </div>
                <div className="col-12">
                    <h4>Retiros</h4>
                </div>
                {retreatsAccount.length === 0 ? (
                    <>
                        <div className="col-12 col-md-5">
                            <p className="mt-0">No hay retiros</p>
                        </div>
                    </>
                ) : (
                    <div
                        className="col-12"
                        style={{ maxWidth: "calc(100vw - 30px)" }}
                    >
                        <Table
                            rows={retreatsAccount}
                            ths={TABLEHEADRETREATS}
                            className="w-100"
                        />
                    </div>
                )}
                <div className="col-12">
                    <h4>Metodos</h4>
                </div>
                {methodsAccount.length === 0 ? (
                    <>
                        <div className="col-12 col-md-5">
                            <p className="mt-0">No hay metodos</p>
                        </div>
                    </>
                ) : (
                    <div
                        className="col-12"
                        style={{ maxWidth: "calc(100vw - 30px)" }}
                    >
                        <Table
                            rows={methodsAccount}
                            ths={TABLEHEADMETHODS}
                            className="w-100"
                        />
                    </div>
                )}
            </div>
        </Content>
    );
};
export const getServerSideProps = async (ctx) => {
    const id = ctx.params.id;
    const query = `?_id=${id}`;
    const config = await getToken(ctx);
    const props = config.props;
    props.account = {};
    props.retreatsAccount = [];
    props.methodsAccount = [];
    props.id = id;
    if (props.token) {
        const token = props.token;
        const result = await accounts.get(token, query);
        if (result.type === "ok") {
            const account = result.respond[0];
            log("ACCOUNTS", account, "aqua");
            if (account) {
                props.account = account;
            }
            const resultRetreats = await retreats.get(token, `?user_id=${id}`);
            if (resultRetreats.type === "ok") {
                const retreatsAccount = resultRetreats.respond;
                log("RETREATS", retreatsAccount, "aqua");
                props.retreatsAccount = retreatsAccount;
            }
            const resultMethods = await methods.get(token, `?user_id=${id}`);
            if (resultMethods.type === "ok") {
                const methodsAccount = resultMethods.respond;
                log("METHODS", methodsAccount, "aqua");
                props.methodsAccount = methodsAccount;
            }
        }
    }

    return config;
};
export default Index;
