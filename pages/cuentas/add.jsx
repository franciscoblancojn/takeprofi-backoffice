import React, { useState, useEffect } from "react";

import getToken from "@/servers/getToken";

import { log, generatePassword, money } from "@/functions/_index";
import { accounts } from "@/api/api";

import Content from "@/components/content";
import Input from "@/components/input";
import Breadcrumbs from "@/components/breadcrumbs";
import Space from "@/components/space";
import Button from "@/components/button";

const ItemsBreadcrumbs = [
    {
        href: "/cuentas/",
        text: "Cuentas",
    },
    {
        href: "/cuentas/add",
        text: "Crear",
    },
];
const Index = ({ token }) => {
    const [respond, setRespond] = useState("");
    const [avalibleSave, setAvalibleSave] = useState(false);
    const [data, setData] = useState({
        email: "",
        password: generatePassword(),
        capitalText: "",
        capital: "",
        porRetiroText: "",
        porRetiro: "",
        cooldown: "",
    });
    const updateData = (id,min=null,max=null) => (e) => {
        var value = e.target.value;
        if(min){
            value = Math.max(min, value)
        }
        if(max){
            value = Math.min(max, value)
        }
        setData({ ...data, [id]: value });
    };
    const createUser = async () => {
        const dataSend = {
            email: data.email,
            password: data.password,
            capital: parseFloat(data.capital),
            porRetiro: parseFloat(data.porRetiro),
            cooldown: parseFloat(data.cooldown),
        };
        const result = await accounts.post(token, dataSend);
        if (result.type == "error") {
            const error = (
                <div className="result error mb-34 color-light-salmon font-size-30">
                    {result.msj || result.msg}
                </div>
            );
            setRespond(error);
            return;
        } else {
            const msj = (
                <div className="result error mb-34 color-seafoam-green font-size-30">
                    User Creado
                </div>
            );
            setRespond(msj);
        }
    };
    const upDown =
        (id) =>
        (value, min = 1, max = 99999999999) =>
        () => {
            var newValue = parseFloat(data[id]) + value;
            if (!newValue) {
                newValue = min;
            }
            newValue = Math.min(max, Math.max(min, newValue));
            setData({ ...data, [id]: newValue });
        };
    useEffect(() => {
        setAvalibleSave(
            data.email != "" &&
                data.password != "" &&
                data.capital != "" &&
                data.porRetiro != "" &&
                data.cooldown != ""
        );
    }, [data]);
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75 pb-20">
                <div className="col-12">
                    <Space space="110" />
                    <h1>Crear Cuentas</h1>
                    <Space space="28" />
                </div>
                <div className="col-12 mb-48">
                    <div className=" flex flex-align-center flex-justify-between w-100">
                        <Breadcrumbs items={ItemsBreadcrumbs} />
                    </div>
                    <Space space="43" />
                    <Input
                        placeholder="Correo Electronico"
                        onChange={updateData("email")}
                    />
                    <Space space="23" />
                    <Input
                        placeholder="Contraseña"
                        defaultValue={data.password}
                        onChange={updateData("password")}
                    />
                    <Space space="23" />
                    <div className="contentInput">
                        <Input
                            placeholder={ money(data.capital).replace("USD", "")}
                            value={data.capital == "" ? "Capital" : ""}
                        />
                        <Input
                            placeholder="Capital"
                            value={data.capital}
                            onChange={updateData("capital",1)}
                            type="number"
                            moreProps={{ min: 1 }}
                            className="overwrite"
                        />
                        <div className="arrows  flex flex-align-center flex-column flex-justify-center">
                            <span
                                className="up"
                                onClick={upDown("capital")(1, 1)}
                            ></span>
                            <span
                                className="down"
                                onClick={upDown("capital")(-1, 1)}
                            ></span>
                        </div>
                    </div>
                    <Space space="23" />
                    <div className="contentInput">
                        <Input
                            placeholder={data.porRetiro + "%"}
                            value={
                                data.porRetiro == ""
                                    ? "Porcentaje de Retiro"
                                    : ""
                            }
                        />
                        <Input
                            placeholder="Porcentaje de Retiro"
                            value={data.porRetiro}
                            onChange={updateData("porRetiro",1,100)}
                            type="number"
                            moreProps={{ min: 1, max: 100 }}
                            className="overwrite"
                        />
                        <div className="arrows  flex flex-align-center flex-column flex-justify-center">
                            <span
                                className="up"
                                onClick={upDown("porRetiro")(1, 1, 100)}
                            ></span>
                            <span
                                className="down"
                                onClick={upDown("porRetiro")(-1, 1, 100)}
                            ></span>
                        </div>
                    </div>
                    <Space space="23" />
                    <div className="contentInput">
                        <Input
                            placeholder="Dias entre Retiros"
                            onChange={updateData("cooldown",1)}
                            type="number"
                            moreProps={{ min: 1 }}
                            value={data.cooldown}
                        />
                        <div className="arrows  flex flex-align-center flex-column flex-justify-center">
                            <span
                                className="up"
                                onClick={upDown("cooldown")(1, 1)}
                            ></span>
                            <span
                                className="down"
                                onClick={upDown("cooldown")(-1, 1)}
                            ></span>
                        </div>
                    </div>
                    <Space space="23" />
                    {respond}
                    <Button
                        text="Crear"
                        onClick={createUser}
                        disabled={!avalibleSave}
                    />
                </div>
            </div>
        </Content>
    );
};
export const getServerSideProps = getToken;
export default Index;
