import React, { useState, useEffect } from "react";
import Link from "next/link";
import exportFromJSON from "export-from-json";

import getToken from "@/servers/getToken";

import { accounts, methods } from "@/api/api";
import { log } from "@/functions/_index";

import { banks, typeCuentas } from "@/const/_index";

import Content from "@/components/content";
import Breadcrumbs from "@/components/breadcrumbs";
import Space from "@/components/space";
import Table from "@/components/table";

const TABLEHEAD = [
    {
        id: "_id",
        html: "ID",
        print: (e) => {
            return (
                <Link href={`/metodos/${e}`}>
                    <a className="font-size-16">{e}</a>
                </Link>
            );
        },
    },
    {
        id: "user_id",
        html: "User ID",
        print: (e) => {
            return (
                <Link href={`/cuentas/${e}`}>
                    <a className="font-size-16">{e}</a>
                </Link>
            );
        },
    },
    {
        id: "name",
        html: "Nombre",
        print: (e) => e,
    },
    {
        id: "account",
        html: "Account",
        print: (e) => e,
    },
    {
        id: "typeAccount",
        html: "Tipo",
        print: (e) => typeCuentas[e] || "",
    },
    {
        id: "bank",
        html: "Banco",
        print: (e) => banks[e].option || "",
    },
    {
        id: "date",
        html: "Fecha",
        print: (e) => {
            return new Date(e).toLocaleDateString();
        },
    },
];
const ItemsBreadcrumbs = [
    {
        href: "/metodos/",
        text: "Metodos",
    },
];
const Index = ({ token, methods }) => {
    log("METHODS", methods, "aqua");
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75 pb-20">
                <div className="col-12">
                    <Space space="110" />
                    <h1>Metodos</h1>
                    <Space space="28" />
                </div>
                <div className="col-12 mb-48">
                    <div className=" flex flex-align-center flex-justify-between w-100">
                        <Breadcrumbs items={ItemsBreadcrumbs} />
                    </div>
                </div>

                {methods.length === 0 ? (
                    <>
                        <div className="col-12 col-md-5">
                            <p className="mt-0">No hay Metodos</p>
                        </div>
                    </>
                ) : (
                    <div
                        className="col-12"
                        style={{ maxWidth: "calc(100vw - 30px)" }}
                    >
                        <Table
                            rows={methods}
                            ths={TABLEHEAD}
                            className="w-100"
                        />
                    </div>
                )}
            </div>
        </Content>
    );
};
export const getServerSideProps = async (ctx) => {
    const config = await getToken(ctx);
    const props = config.props;
    props.methods = [];
    if (props.token) {
        const token = props.token;
        const resultMethods = await methods.get(token);
        if (resultMethods.type === "ok") {
            const methods = resultMethods.respond;
            log("METHODS", methods, "aqua");
            props.methods = methods;
        }
    }
    return config;
};
export default Index;
