import React, { useState, useEffect } from "react";
import Link from "next/link";
import exportFromJSON from "export-from-json";

import getToken from "@/servers/getToken";

import { accounts, retreats, methods } from "@/api/api";

import { money, log } from "@/functions/_index";

import { banks, typeCuentas } from "@/const/_index";

import Content from "@/components/content";
import Breadcrumbs from "@/components/breadcrumbs";
import Space from "@/components/space";
import Table from "@/components/table";

const ItemsBreadcrumbs = [
    {
        href: "/retiros/",
        text: "Retiros",
    },
];
const Index = ({ token, retreats, metodos }) => {
    log("METHODS", retreats, "aqua");
    const metodosSelect = {};
    metodos.forEach((element) => {
        metodosSelect[element._id] = {
            text: element.account,
            option: (
                <Link href={`/metodos/${element._id}`}>
                    <a className="flex flex-no-wrap flex-align-center font-size-16">
                        <img
                            src={`/banks/${element.bank}.png`}
                            alt={element.bank}
                            className="mr-5"
                        />
                        {element.name}
                    </a>
                </Link>
            ),
        };
    });
    const TABLEHEAD = [
        {
            id: "_id",
            html: "ID",
            print: (e) => {
                return (
                    <Link href={`/retiros/${e}`}>
                        <a className="font-size-16">{e}</a>
                    </Link>
                );
            },
        },
        {
            id: "user_id",
            html: "User ID",
            print: (e) => {
                return (
                    <Link href={`/cuentas/${e}`}>
                        <a className="font-size-16">{e}</a>
                    </Link>
                );
            },
        },
        {
            id: "date",
            html: "Fecha",
            print: (e) => {
                return new Date(e).toLocaleDateString();
            },
        },
        {
            id: "methods",
            html: "Metodo",
            print: (e) => {
                return metodosSelect[e].option;
            },
        },
        {
            id: "monto",
            html: "Monto",
            print: (e) => {
                return money(e);
            },
        },
    ];
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75 pb-20">
                <div className="col-12">
                    <Space space="110" />
                    <h1>Retiros</h1>
                    <Space space="28" />
                </div>
                <div className="col-12 mb-48">
                    <div className=" flex flex-align-center flex-justify-between w-100">
                        <Breadcrumbs items={ItemsBreadcrumbs} />
                    </div>
                </div>

                {retreats.length === 0 ? (
                    <>
                        <div className="col-12 col-md-5">
                            <p className="mt-0">No hay Retiros</p>
                        </div>
                    </>
                ) : (
                    <div
                        className="col-12"
                        style={{ maxWidth: "calc(100vw - 30px)" }}
                    >
                        <Table
                            rows={retreats}
                            ths={TABLEHEAD}
                            className="w-100"
                        />
                    </div>
                )}
            </div>
        </Content>
    );
};
export const getServerSideProps = async (ctx) => {
    const config = await getToken(ctx);
    const props = config.props;
    props.retreats = [];
    props.metodos = [];
    if (props.token) {
        const token = props.token;
        const resultRetreats = await retreats.get(token);
        if (resultRetreats.type === "ok") {
            const retreatsAccount = resultRetreats.respond;
            log("RETREATS", retreatsAccount, "aqua");
            props.retreats = retreatsAccount;
        }
        const resultM = await methods.get(token);
        if (resultM.type === "ok") {
            props.metodos = resultM.respond;
        }
        log("METHODS", props.metodos, "aqua");
    }

    return config;
};
export default Index;
