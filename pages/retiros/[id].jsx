import React, { useState, useEffect } from "react";
import Link from "next/link";
import exportFromJSON from "export-from-json";

import getToken from "@/servers/getToken";

import { accounts, retreats } from "@/api/api";
import { log,money } from "@/functions/_index";

import { banks, typeCuentas } from "@/const/_index";

import Content from "@/components/content";
import Breadcrumbs from "@/components/breadcrumbs";
import Space from "@/components/space";
import Table from "@/components/table";

const HEADRETREAT = [
    {
        id: "date",
        html: "Fecha",
        print: (e) => {
            return new Date(e).toLocaleDateString();
        },
    },
    {
        id: "account",
        html: "Cuenta",
        print: (e) => {
            return (
                <div className="flex flex-no-wrap flex-align-center">
                    <img
                        src={`/banks/${e.bank}.png`}
                        alt={e.bank}
                        className="mr-10"
                    />
                    <div className="info align-left">
                        <strong className="name color-black-two font-size-22">
                            {e.name}
                        </strong>
                        <div className="number color-brownish-grey font-size-25">
                            {e.account}
                        </div>
                        <div className="type color-greyish-two font-size-19">
                            {typeCuentas[e.type]}
                        </div>
                    </div>
                </div>
            );
        },
    },
    {
        id: "periodo",
        html: "Periodo",
        print: (e) => {
            return `Periodo ${e}`;
        },
    },
    {
        id: "capital",
        html: "Capital Consolidado",
        print: (e) => {
            return money(e);
        },
    },
    {
        id: "monto",
        html: "Monto",
        print: (e) => {
            return money(e);
        },
    },
];
const Index = ({ token, retreat }) => {
    log("retreat", retreat, "aqua");
    const ItemsBreadcrumbs = [
        {
            href: "/retiros/",
            text: "Retiros",
        },
        {
            href: `/retreats/${retreat._id}`,
            text: retreat._id,
        },
    ];
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75 pb-20">
                <div className="col-12">
                    <Space space="110" />
                    <h1>Retiros</h1>
                    <Space space="28" />
                </div>
                <div className="col-12 mb-48">
                    <div className=" flex flex-align-center flex-justify-between w-100">
                        <Breadcrumbs items={ItemsBreadcrumbs} />
                    </div>
                </div>
                <div className="col-12">
                    <ul>
                        {HEADRETREAT.map((e, i) => {
                            return (
                                <li key={i} className="mb-10">
                                    <div className="flex flex-align-center">
                                        <strong className="mr-10">
                                            {e.html}:{" "}
                                        </strong>
                                        {e.print(retreat[e.id])}
                                    </div>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        </Content>
    );
};
export const getServerSideProps = async (ctx) => {
    const id = ctx.params.id;
    const query = `?_id=${id}`;
    const config = await getToken(ctx);
    const props = config.props;
    props.retreat = {};
    if (props.token) {
        const token = props.token;
        const resultRetreats = await retreats.get(token, query);
        if (resultRetreats.type === "ok") {
            const retreats = resultRetreats.respond;
            log("METHODS", retreats, "aqua");
            props.retreat = retreats[0];
        }
    }
    return config;
};
export default Index;
