import Link from "next/link";

const Index = (props) => {
    const { items } = {
        items: [],

        ...props,
    };
    return (
        <div className="breadcrumbs">
            {items.map((e, i) => {
                return (
                    <Link href={e.href} key={i}>
                        <a className="color-warm-grey font-size-24">{e.text}</a> 
                    </Link>
                );
            })}
        </div>
    );
};
export default Index;
