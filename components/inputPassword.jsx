import React, { useRef,useState } from "react";

const Index = (props) => {
    const [show, setShow] = useState(false);
    const {
        placeholder,
        className,
        classNameContent,
        onChange,
        defaultValue,
        disabled,
        value,
    } = {
        placeholder: "",
        className: "",
        classNameContent : "",
        onChange: (e) => console.log(e),
        defaultValue : "",
        disabled : false,
        value : null,

        ...props,
    };
    const add = {}
    if(value !==  null){
        add.value = value
    }else{
        add.defaultValue = defaultValue
    }
    return (
        <div className={`contentInput ${classNameContent}`}>
            <input
                className={`input ${className}`}
                type={show?"text":"password"}
                placeholder={placeholder}
                onChange={onChange}
                disabled={disabled}
                {...add}
            />

            <svg
                width="2.8rem"
                viewBox="0 0 46 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                className="icon"
                onClick={()=>{setShow(!show)}}
            >
                <path
                    d="M22.9302 10.047C21.7202 10.047 20.5502 10.404 19.5502 11.073C18.5502 11.741 17.7602 12.692 17.3002 13.804C16.8402 14.916 16.7201 16.14 16.9601 17.32C17.1901 18.501 17.7701 19.585 18.6201 20.436C19.4701 21.288 20.5601 21.867 21.7401 22.102C22.9201 22.337 24.1401 22.216 25.2601 21.756C26.3701 21.295 27.3201 20.515 27.9901 19.514C28.6601 18.513 29.0101 17.337 29.0101 16.133C29.0101 14.519 28.3701 12.972 27.2301 11.831C26.0901 10.69 24.5402 10.048 22.9302 10.047Z"
                    fill="var(--greyish-two)"
                />
                <path
                    d="M22.9299 0.917969C18.1099 0.920969 13.3999 2.37395 9.41992 5.08795C5.43992 7.80195 2.36986 11.6509 0.609863 16.1339C2.36986 20.6189 5.43992 24.47 9.41992 27.184C13.3999 29.899 18.1099 31.3499 22.9299 31.3499C27.7499 31.3499 32.4499 29.899 36.4299 27.184C40.4099 24.47 43.4799 20.6189 45.2399 16.1339C43.4799 11.6499 40.4099 7.79994 36.4299 5.08594C32.4499 2.37194 27.7399 0.918969 22.9299 0.917969ZM22.9299 26.278C20.9199 26.278 18.9599 25.6829 17.2899 24.5679C15.6199 23.4539 14.3199 21.8689 13.5599 20.0159C12.7899 18.1619 12.5899 16.123 12.9799 14.155C13.3699 12.187 14.3399 10.3799 15.7499 8.96094C17.1699 7.54194 18.9798 6.57594 20.9498 6.18494C22.9198 5.79294 24.9599 5.99396 26.8099 6.76196C28.6599 7.52996 30.2499 8.82992 31.3599 10.4979C32.4799 12.1659 33.0698 14.1279 33.0698 16.1339C33.0698 18.8239 31.9999 21.404 30.0999 23.306C28.1999 25.208 25.6199 26.277 22.9299 26.278Z"
                    fill="var(--greyish-two)"
                />
            </svg>
        </div>
    );
};
export default Index;
