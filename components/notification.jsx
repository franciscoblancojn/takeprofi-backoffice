import React, { useState, useEffect } from "react";
import socketIOClient from "socket.io-client";

import { money, log } from "@/functions/_index";

const Index = () => {
    const [notifications, setNotifications] = useState([]);
    const pushNotification = (newNotification) => {
        log("new Notification", newNotification, "blue");
        setNotifications((oldArray) => [...oldArray, newNotification]);
    };
    useEffect(() => {
        const socket = socketIOClient(
            process.env.URLAPI.replace("/api/v1/", "")
        );
        socket.on("[POST] /retreats", (data) => {
            log("[POST] /retreats", data, "green");
            pushNotification({
                id: data._id,
                className: "bg-seafoam-green color-midnight-blue",
                msj: `Solicitud de Retiro del usuario ${data.user_id}`,
            });
        });
    }, []);
    useEffect(() => {
        log("notifications", notifications);
    }, [notifications]);
    return (
        <div className={`contentNotification ${notifications.length}`}>
            {notifications.map((e, key) => (
                <div
                    key={key}
                    className={`notification font-size-25  ${e.className}`}
                >
                    {e.msj}
                </div>
            ))}
        </div>
    );
};
export default Index;
