import React, { useState, useEffect } from "react";
import Link from "next/link";
import Cookies from "js-cookie";

import { money, log } from "@/functions/_index";

import Notification from "@/components/notification";

const Index = (props) => {
    const [active, setActive] = useState(true);
    const { exampleProps } = {
        exampleProps: "defaulProps",

        ...props,
    };
    const toggleMenu = () => {
        document.body.classList.toggle("activeMenu");
        setActive(!active);
    };
    const clickIcon = (e) => {
        var li = e.target.parentElement;
        while (li.localName !== "li") {
            li = li.parentElement;
        }
        li.classList.toggle("active");
    };
    const liActive = (e) => {
        if (e.target.localName === "li") {
            e.target.classList.toggle("active");
        }
    };
    const clickMenu = () => {
        if (window.innerWidth <= 767) {
            setActive(false);
            document.body.classList.remove("activeMenu");
        }
    };
    const salir = () => {
        toggleMenu();
        Cookies.remove("token");
    };
    useEffect(() => {
        // document.body.classList.add("activeMenu");
        if (window.innerWidth <= 767) {
            setActive(false);
            document.body.classList.remove("activeMenu");
        } else {
            document.body.classList.add("activeMenu");
        }
    }, []);
    return (
        <>
            <Notification />
            <header className={`nav navLef ${active ? "active" : ""}`}>
                <svg
                    viewBox="0 0 57 36"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="iconMenu"
                    onClick={toggleMenu}
                >
                    <path
                        d="M1.00012 1.73301C1.00012 1.27339 1.18271 0.832596 1.50771 0.507595C1.83271 0.182595 2.2735 1.17601e-05 2.73312 1.17601e-05H54.7141C54.9451 -0.00553884 55.1749 0.0351683 55.39 0.11974C55.605 0.204311 55.801 0.331039 55.9664 0.492466C56.1317 0.653893 56.2631 0.84676 56.3528 1.05972C56.4425 1.27268 56.4887 1.50143 56.4887 1.73251C56.4887 1.96359 56.4425 2.19234 56.3528 2.4053C56.2631 2.61826 56.1317 2.81113 55.9664 2.97255C55.801 3.13398 55.605 3.26071 55.39 3.34528C55.1749 3.42985 54.9451 3.47056 54.7141 3.46501H2.73312C2.27368 3.46501 1.83303 3.28257 1.50806 2.95778C1.18309 2.633 1.00039 2.19246 1.00012 1.73301ZM54.7141 16.127H2.73312C2.5021 16.1215 2.27231 16.1622 2.05726 16.2467C1.84221 16.3313 1.64624 16.458 1.48089 16.6195C1.31553 16.7809 1.18414 16.9738 1.09442 17.1867C1.00471 17.3997 0.958496 17.6284 0.958496 17.8595C0.958496 18.0906 1.00471 18.3193 1.09442 18.5323C1.18414 18.7453 1.31553 18.9381 1.48089 19.0996C1.64624 19.261 1.84221 19.3877 2.05726 19.4723C2.27231 19.5569 2.5021 19.5976 2.73312 19.592H54.7141C54.9451 19.5976 55.1749 19.5569 55.39 19.4723C55.605 19.3877 55.801 19.261 55.9664 19.0996C56.1317 18.9381 56.2631 18.7453 56.3528 18.5323C56.4425 18.3193 56.4887 18.0906 56.4887 17.8595C56.4887 17.6284 56.4425 17.3997 56.3528 17.1867C56.2631 16.9738 56.1317 16.7809 55.9664 16.6195C55.801 16.458 55.605 16.3313 55.39 16.2467C55.1749 16.1622 54.9451 16.1215 54.7141 16.127ZM28.7231 32.254H2.73312C2.28084 32.2649 1.85074 32.4522 1.5347 32.7759C1.21866 33.0996 1.04175 33.5341 1.04175 33.9865C1.04175 34.4389 1.21866 34.8734 1.5347 35.1971C1.85074 35.5208 2.28084 35.7081 2.73312 35.719H28.7231C29.1754 35.7081 29.6055 35.5208 29.9215 35.1971C30.2376 34.8734 30.4145 34.4389 30.4145 33.9865C30.4145 33.5341 30.2376 33.0996 29.9215 32.7759C29.6055 32.4522 29.1754 32.2649 28.7231 32.254Z"
                        fill="var(--color-menu)"
                    />
                </svg>
                <div className="menu flex flex-space-between">
                    <div className="w-100">
                        <div className="top">
                            <Link href="/">
                                <a onClick={clickMenu}>
                                    <img
                                        src="/img/logoWhite.svg"
                                        alt="TakeProfit"
                                    />
                                </a>
                            </Link>
                        </div>

                        <div className="navigationMenu" onClick={liActive}>
                            <ul className="ulFirst">
                                <li>
                                    <Link href="/">
                                        <a onClick={clickMenu}>Inicio</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/cuentas">
                                        <a onClick={clickMenu}>Cuentas</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/metodos">
                                        <a onClick={clickMenu}>Metodos</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/retiros">
                                        <a onClick={clickMenu}>Retiros</a>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="down w-100">
                        <Link href="/login">
                            <a onClick={salir}>Salir</a>
                        </Link>
                    </div>
                </div>
            </header>
        </>
    );
};
export default Index;
