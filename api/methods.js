import request from "@/api/request";

const get = async (token,query="") => {
    const result = await request({
        method: "get",
        url: `${process.env.URLAPI}methods/admin${query}`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
    return result;
}
const post = async (token,data) => {
    const result = await request({
        method: "post",
        url: `${process.env.URLAPI}methods`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        data: JSON.stringify(data)
    });
    return result;
}

const put = async (token,_id,data) => {
    const result = await request({
        method: "put",
        url: `${process.env.URLAPI}methods?_id=${_id}`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        data: JSON.stringify(data)
    });
    return result;
}

export default {
    get,
    post,
    put,
}