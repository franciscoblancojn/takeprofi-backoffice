import request from "@/api/request";

const auth = async ({ email, password }) => {
    const result = await request({
        method: "post",
        url: `${process.env.URLAPI}auth/admin`,
        headers: {
            apikey: process.env.APIKEY,
            "Content-Type": "application/json",
        },
        data: JSON.stringify({
            email,
            password,
        }),
    });
    return result;
};
export default auth;
