import axios from "axios";
import { log, expireToken } from "@/functions/_index";

const request = async (config) => {
    try {
        const response = await axios(config);
        log("RESPOND REQUEST", response, "green");
        return response.data;
    } catch (error) {
        log("ERROR REQUEST", error, "red");
        log("ERROR REQUEST Response", error.response, "red");
        log("ERROR REQUEST data", (error.response || {}).data, "red");
        if (((error.response || {}).data || {}).msj === "TokenExpiredError: jwt expired") {
            expireToken();
        }
        return (error.response || {}).data;
    }
};
export default request;
