import request from "@/api/request";

const get = async (token,query="") => {
    const result = await request({
        method: "get",
        url: `${process.env.URLAPI}retreats/admin${query}`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
    return result;
}
export default {
    get,
}