import auth_ from "@/api/auth";
export const auth = auth_;

import methods_ from "@/api/methods";
export const methods = methods_;

import retreats_ from "@/api/retreats";
export const retreats = retreats_;

import accounts_ from "@/api/accounts";
export const accounts = accounts_;