import request from "@/api/request";

const get = async (token,query="") => {
    const result = await request({
        method: "get",
        url: `${process.env.URLAPI}accounts${query}`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
    return result;
}
const post = async (token,data) => {
    const result = await request({
        method: "post",
        url: `${process.env.URLAPI}accounts`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        data: JSON.stringify(data)
    });
    return result;
}
const put = async (token,data) => {
    const result = await request({
        method: "put",
        url: `${process.env.URLAPI}accounts`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        data: JSON.stringify(data)
    });
    return result;
}
export default {
    get,
    post,
    put,
}